package collection;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Task1 {
    public static void main(String[] args) {
        UtilCollection<Integer> utilCollection = new UtilCollection<>();
        List<Integer> list = List.of(5, 2, 8, 8, 2, 5);
        System.out.println("Исходная данные:" + list);
        System.out.print("Уникальные значения: ");
        Set<Integer> set = utilCollection.getUniqueCollection(list);
        System.out.println(set);
    }
}


class UtilCollection<T extends Number> {

    public Set<T> getUniqueCollection(List<T> list){
        return new HashSet<>(list);

    }

}