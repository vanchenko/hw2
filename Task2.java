package collection;

import java.util.Arrays;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первую строку");;
        String s = scanner.nextLine();
        System.out.println("Введите вторую строку");;
        String t = scanner.nextLine();
        System.out.println("Введенная строка является анаграммой второй строке: " + isStringAnagram(s, t));
    }


    public static boolean isStringAnagram(String string1, String string2){
        char[] a1 = string1.toCharArray();
        char[] a2 = string2.toCharArray();
        Arrays.sort(a1);
        Arrays.sort(a2);
        return Arrays.equals(a1, a2);
    }
}
