package collection;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet{

    public <T> Set<T> intersection(Set<T> set1, Set<T> set2){
        Set<T> result = new HashSet<>(set1);
        result.retainAll(set2);
        return result;
    }

    public <T> Set<T> union(Set<T> set1, Set<T> set2){
        Set<T> result = new HashSet<>(set1);
        result.addAll(set2);
        return result;
    }

    public <T> Set<T> relateCompliment(Set<T> set1, Set<T> set2){
        Set<T> result = new HashSet<>(set1);
        result.removeAll(set2);
        return result;
    }
}

class Main {
    public static void main(String[] args) {
        PowerfulSet powerfulSet = new PowerfulSet();
        System.out.println("Исходные множества:");
        Set<Integer> set1 = Set.of(1, 2, 3);
        Set<Integer> set2 = Set.of(0, 1, 2, 4);
        System.out.println("Set1: " + set1);
        System.out.println("Set2: " + set2);

        System.out.print("Результат пересечения: ");
        System.out.println(powerfulSet.intersection(set1, set2));

        System.out.print("Результат объединения: ");
        System.out.println(powerfulSet.union(set1, set2));

        System.out.print("Результат разности: ");
        System.out.println(powerfulSet.relateCompliment(set1, set2));
    }
}
