package collection;

import java.util.*;
import java.util.stream.Collectors;

public class Task4 {
    public static void main(String[] args) {
        List<Document> documents = new ArrayList<>();
        documents.add(new Document(2, "222", 5));
        documents.add(new Document(1, "123", 10));
        documents.add(new Document(3, "333", 8));

        UtilArchive utilArchive = new UtilArchive();
        Map<Integer, Document> map = utilArchive.organizeDocuments1(documents);

        System.out.println("Результат организации документов (id, данные документа)");
        for (Map.Entry<Integer, Document> element : map.entrySet()) {
            System.out.println(element.getKey() + " --- " + element.getValue());
        }
    }
}

class Document{
    public int id;
    public String number;
    public int pageCount;

    public Document(int id, String number, int pageCount) {
        this.id = id;
        this.number = number;
        this.pageCount = pageCount;
    }



    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", pageCount=" + pageCount +
                '}';
    }
}

class UtilArchive {
    // 1 вариант
    public Map<Integer, Document> organizeDocuments1(List<Document> documents){
        Map<Integer, Document> map = documents.stream()
                .collect(Collectors.toMap(
                        document -> document.id,
                        document -> document
                ));
        return map;
    }

    // 2 вариант
    public Map<Integer, Document> organizeDocuments2(List<Document> documents){
        Map<Integer, Document> map = new HashMap<>();

        for (Document document : documents) {
            map.put(document.id, document);
        }
        return map;
    }

}